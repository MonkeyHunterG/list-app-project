#include <iostream>
#include <vector>
using namespace std;

void print_menu(string name);
void print_list();
void add_item();
void delete_item();

vector<string> list;
string name;

int main(int arg_count, char *args[]){
    if(arg_count > 1){
        string name(args[1]);
        print_menu(name);
    }else{
        cout << "No arguments given!" << endl;
    }
    return 0;
}

void print_menu(string name){
    int choice;
    cout << "1 - Print list\n";
    cout << "2 - Add to list\n";
    cout << "3 - Delete from list\n";
    cout << "4 - Quit prog\n";
    cout << "Pls enter choice and press return:";
    
    cin >> choice;
    if(choice==4){
        exit(0);
    }else if(choice == 1){
        print_list();
    }else if(choice == 2){
        add_item();
    }else if(choice == 3){
        delete_item();
    }else{
        cout << "not in yet\n";
    }
}

void add_item(){
    string item;
    cout << "\n\n\n";
    cout << "*** Add item\n";
    cout << "Type in item and press return: ";
    //getline(cin, item);
    cin >> item;
    list.push_back(item);
    cout << "Item added\n";
    cin.clear();
    print_menu(name);
}

void delete_item(){
    cout << "*** Delete item\n";
    cout << "Select item index to delete that item\n";
    if(list.size()){
        for(int i=0;i<list.size();i++){
            cout << i << ": " << list[i] << "\n";
        }
    }else{
        cout << "No items to delete\n";
    }
    print_menu(name);
}

void print_list(){
    cout << "\n\n\n";
    cout << "*** Print list\n";
    for(int j=0;j<list.size();j++){
        cout << " * " << list[j] << "\n";
    }
    cout << "M - Menu\n";
    char choice;
    cin >> choice;
    if(choice == 'M' || choice == 'm'){
        print_menu(name);
    }else{
        cout << "Invalid choice. Quitting.\n";
    }
}